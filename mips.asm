.data
pinakasa:.space 120
pinakasb:.space 120
pinakasc:.space 120
minima:.asciiz "dose ton char c gia antigrafi tou pinaka_a ston pinaka_b h ton char s gia ta3inomisi tou pinaka_a ston pinaka_c "
nchar:.space 4
chars:.asciiz "s"
charc:.asciiz "c"



.text
main:
				add $3,$0,$0	#metakinisis gia tin ekxorisi diabasmenwn ari8mon
				sub $5,$0,99	#sygkrisis stin periptosi pou do8ei to -99
				addi $6,$0,120  #telos epanalipseon
				addi $10,$0,115 #to "s" ston ascii
				addi $11,$0,99	#to "c" ston ascii
gemisma:   		addi $2,$0,5
				syscall
				sw $2,pinakasa($3)  #topo8etisi stoixeiou ston pinakaA
				addi $3,$3,4
				beq $2,$5,ext		#exit stin periptosi pou do8ei -99	
				bne $3,$6,gemisma	#syn8iki ean exei ftasei stous 30 akeraious

				
ext:			la $4,minima	    #ektiposi tou minimatos
				addi $2,$0,4		
				syscall
wrong_char:		la $4,nchar
				addi $2,$0,8	#diabasma tou char "c" h "s"
				syscall
				lw $20,nchar($0)
				jal function_char
				jal function_s
its_c:			jal function_c		#paei stin function_c

				
				j main






function_char:	bne $20,$10,not_s	#elegxei ean einai to s
				jr $31
			
not_s:			bne $20,$11,wrong_char	#elegxei ean einai to "c" ean dn einai paei na ksanadosei char	
				j its_c					#ean einai "c" apla na paei sto lable its_c	

function_s:		addi $8,$0,0		#bima
				addi $9,$0,4		#bima
				addi $15,$0,0		#dipli mou epanalipsi gia tin swsti ta3inomisi
		
again:			addi $0,$0,0

epomena:		lw $12,pinakasa($8)	#fernw ta prota 2 stoixeia
				lw $13,pinakasa($9)	
				slt $14,$12,$13		#an to periexomeno den tou $12<$13 o $14 tha parei timi 0
				bne $14,$0,change	#afou parei 0 kai einai iso me to 0 paei na ta alla3ei
				addi $8,$8,4		#alliws au3anei kata 4 kai paei sta epomena stoixeia
				addi $9,$9,4
				bne $9,$3,epomena
ektupwsi_s:		lw $4,pinakasa($16)		#pairnw to stoixeio apo ton pinaka A
				lw $18,pinakasc($16)	#pairnw to stoixeio apo ton pinaka C
				addi $2,$0,1			#ektupwsi tou pinakaa
				syscall
				add $4,$0,$18			
				addi $2,$0,1
				syscall					#ektupwsi tou pinakac
				addi $16,$16,4
				bne $16,$3,ektupwsi_s
telos_s:		jr $31	


change:			sw $13,pinakasc($8)	#allazei tis thesis tous
				sw $12,pinakasc($9)
				addi $8,$8,4		#bima++
				addi $9,$9,4		#bima++
				bne $9,$3,epomena	#meta tis allages paei sta epomena
				addi $15,$15,4		#epeidi xreiazete dipli epanalipsi
				bne $15,$3,again	#to $3 mas deixnei posous ari8mous exoume diabasei
				j telos_s			#an teleiosoun ta stoixeia kleinei tin funtion gia to "s"

function_c:		addi $16,$0,0
		
antigrafi:		lw $17,pinakasa($16)	#pairnw to stoixeio apo ton pinaka A
				sw $17,pinakasb($16)	#antigrafoume ston pinaka B stin antistoixi thesi
				addi $16,$16,4			#ay3anw gia na parw to epomeno stoixeio
				bne $16,$3,antigrafi	#kanoume ksana tin diadikasia
				addi $16,$0,0
ektupwsi_c:		lw $4,pinakasa($16)		#pairnw to stoixeio apo ton pinaka A
				lw $18,pinakasb($16)	#pairnw to stoixeio apo ton pinaka B
				addi $2,$0,1			#ektupwsi tou Pinakaa
				syscall
				add $4,$0,$18			
				addi $2,$0,1
				syscall					#ektupwsi tou Pinakab
				addi $16,$16,4
				bne $16,$3,ektupwsi_c
				jr $31					#pisw stin main